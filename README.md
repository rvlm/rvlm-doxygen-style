Rvlm style for Doxygen documentation
====================================
This project was created to provide default style definitions for
project Ravenholm and its subprojects, with nicer and cleaner documentation
look compared to original Doxygen style.

The original source code was taken from Ross Cairns's project "Doxygen Style+"
which can be found on https://bitbucket.org/rc1/doxygen-style. Because the
original project uses Mercurial, there was no possibility for making a proper
fork, in the technical sense of this word.
